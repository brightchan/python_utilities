__author__ = 'Bright'

#Find linear probes with specific total length, GC content and stem length

def GCcount(s):
    s = s.translate(None,'.`').upper()
    GC = 0
    for i in xrange(len(s)):
        if s[i]=='G' or s[i]=='C':
            GC += 1
    return GC

def Gcount(s):#rules for G numbers
    s = s.translate(None,'.`').upper()
    G = 0
    if 'GGG' in s:
        return False
    for i in xrange(len(s)):
        if s[i]=='G' or s[i]=='g':
            G += 1
    if G > 9:
        return False
    else:
        return True

def revcom(DNA):#only for uppercase
    l = ''
    for i in range(len(DNA)-1,-1,-1):
        if DNA[i] == 'A':
            l +=('T')
        if DNA[i] == 'T':
            l +=('A')
        if DNA[i] == 'C':
            l +=('G')
        if DNA[i] == 'G':
            l +=('C')
    return l

def find_linear_probes(seq):
    o = []
    for i in range(len(seq)-19):
        tem = seq[i:i+20]
        if Gcount(tem) and GCcount(tem)>10 and GCcount(tem[-17:])>8 and GCcount(tem[-1])==1:
            o.append((seq.index(tem),tem,tem[-17:],revcom(tem)))
    return o

if __name__ =='__main__':
    DNA = ''' tgccccga caggaagtgg
     1381 atggattttt aatgcactcc tgtaaatgtt tacaaactgt tgattcgtgt atcaggtgag
     1441 aggagacgtg acaccaaaga ggcttgggaa attaccacca tgtgcaccgc tgtgtaactg
     1501 ttcaggcaaa cggttgattt tcaaaaatga atgaagtatt taagttttat caaaccacat
     1561 tagcctccgt gacgctttag gactcccatg ttttgatgtt tcctctgctt ttttccgcac
     1621 aggttttcta tattagatca actgtagacg gtttagccag actctctgtt agctacaggt
     1681 ttttgtacaa catctaaaga acaattgaca tcacatttat acatgttttt atgtggaata
     1741 gctgtcgctc aaacttttta agatgtcaaa catttcttta ataaaaactg taatacttgt
     1801 agctctgtca aaaaaaaaaa aaaaaaaaa'''
    DNA = revcom(DNA.upper())
    out = find_linear_probes(DNA)
    for i in out:
        print '>',i[0],'\n',i[1]