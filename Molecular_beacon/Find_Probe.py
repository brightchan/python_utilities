inp = open('input.txt','r')
outp = open('output.txt','w')
DNA = inp.read().upper()

def GCcount(s):
    s = s.translate(None,'.`').upper()
    GC = 0
    for i in xrange(len(s)):
        if s[i]=='G' or s[i]=='C':
            GC += 1
    return GC

def Gcount(s):#rules for G numbers
    s = s.translate(None,'.`').upper()
    G = 0
    if 'GGG' in s:
        return False
    for i in xrange(len(s)):
        if s[i]=='G' or s[i]=='g':
            G += 1
    if G > 8:
        return False
    else:
        return True

def revcom(DNA):#only for uppercase
    l = ''
    for i in range(len(DNA)-1,-1,-1):
        if DNA[i] == 'A':
            l +=('T')
        if DNA[i] == 'T':
            l +=('A')
        if DNA[i] == 'C':
            l +=('G')
        if DNA[i] == 'G':
            l +=('C')
    return l

def SelfCom(s):#return the No. of self-comlementary nt
    s1 = s.translate(None,'.`').upper()
    s2 = revcom(s1)
    sc=0
    for i in xrange((len(s1)+1)/2):
        if s1[i]==s2[i]:
            sc+=1
    return str(sc)

def findProbe(seq,sl,ll):#find MB with set stem length(sl) and loop length (ll)
    dic = {}
    revstem = ''
    MB = ''
    for i in xrange(len(seq)-sl*2-ll):
        if seq[i]=='G':
            if GCcount(seq[i:i+sl])>5:#set stem GC%
                j = i + sl -1
                k = i + sl + ll
                revstem = 0
                while seq[j] == revcom(seq[k]):
                    j -= 1
                    k += 1
                    revstem += 1
                if revstem>0:
                    MB = seq[i:i+ sl] + '`' + seq[i+sl:i+sl+ll]  + seq[i+sl+ll:i+sl+ll+revstem].lower() + '.' + revcom(seq[i:i+ sl - revstem]).lower()
                    GCp = GCcount(MB)/25.000
                    if Gcount(MB) and GCp > 0.70 and GCp < 0.75: #set GC%
                        MBrev = revcom(seq[i:i+sl+ll+revstem])
                        dic[MB] = str(len(seq)- i - sl - ll) +' ' + SelfCom(MB)+ ' ' + str(100*GCp) + '% ' + MBrev
    return dic



if __name__ =='__main__':
    DNA = revcom(DNA)
    out = findProbe(DNA,10,5)
    p = []
    MBe = []
    for k,v in out.items():#print result
        print k + ' ' + v
        p.append(int(v.split()[0]))


    #Below for MB across exons
    #    vasexon = open('olvas exon.txt','r')
    #exonp = []
    #for x in vasexon.read().split():
    #exonp.append(int(x))
    # for i in exonp:
    #     for j in p:
    #         if i > j-1 and i < j + 20:
    #             MBe.append(str(j))


    for k,v in out.items():
        if v.split()[0] in MBe:
            print '>' + v.split()[0] + '\n' + k

inp.close()
outp.close()
