inp = open('Rx2.txt','r')
outp = open('output.txt','w')

def GCcount(s):
    s = s.translate(None,'.`').upper()
    GC = 0
    for i in xrange(len(s)):
        if s[i]=='G' or s[i]=='C':
            GC += 1
    return GC

def Gcount(s):#rules for G numbers
    s = s.translate(None,'.`').upper()
    G = 0
    if 'GGG' in s:
        return False
    for i in xrange(len(s)):
        if s[i]=='G' or s[i]=='g':
            G += 1
    if G > 9:
        return False
    else:
        return True

def revcom(DNA):#only for uppercase
    l = ''
    for i in range(len(DNA)-1,-1,-1):
        if DNA[i] == 'A':
            l +=('T')
        if DNA[i] == 'T':
            l +=('A')
        if DNA[i] == 'C':
            l +=('G')
        if DNA[i] == 'G':
            l +=('C')
    return l

def SelfCom(s):#return the No. of self-comlementary nt
    s1 = s.translate(None,'.`').upper()
    s2 = revcom(s1)
    sc=0
    for i in xrange((len(s1)+1)/2):
        if s1[i]==s2[i]:
            sc+=1
    return str(sc)

def findMB(seq,sl,ll):#find MB with set stem length(sl) and loop length (ll)
    dic = {}
    revstem = ''
    MB = ''
    for i in xrange(len(seq)-sl*2-ll):
        if seq[i]=='G':
            if GCcount(seq[i:i+sl])>4:#set stem GC%
                j = i + sl -1
                k = i + sl + ll
                revstem = 0
                mismatch = 0
                mislist = []
                while mismatch < 2:#find reverse stem on original sequence allowing mismatch to target site
                    if seq[j] <> revcom(seq[k]):
                        mismatch += 1
                        mislist.append(k)
                    j -= 1
                    k += 1
                    revstem += 1
                revstem -=1
                if revstem>2 and mislist[1]-mislist[0]>1:
                    revstemseq = ''
                    for k in xrange(i+sl+ll,i+sl+ll+revstem,1):
                        if k not in mislist:
                            revstemseq += seq[k].lower()
                        else:
                            revstemseq += revcom(seq[i+2*sl+ll-k+i-1])
                    MB = seq[i:i+ sl] + '`' + seq[i+sl:i+sl+ll]  + revstemseq + '.' + revcom(seq[i:i+ sl - revstem]).lower()
                    GCp = GCcount(MB)/float(sl*2+ll)
                    if Gcount(MB) and GCp > 0.40 and GCp < 0.75: #set GC%
                        MBrev = revcom(seq[i:i+sl+ll+revstem])
                        dic[MB] = str(len(seq)- i - sl - ll) +' '  + str(revstem) + ' ' + SelfCom(MB)+ ' ' + str(round(100*GCp,2)) + '% ' + MBrev
    return dic



if __name__ =='__main__':
    DNA = inp.read().upper()
    DNA = revcom(DNA)
    out = findMB(DNA,10,5)
    p = []
    MBe = []
    for k,v in out.items():#print result
        print k + ' ' + v
        p.append(int(v.split()[0]))

    #Below for MB across exons
    #    vasexon = open('olvas exon.txt','r')
    #exonp = []
    #for x in vasexon.read().split():
    #exonp.append(int(x))
    # for i in exonp:
    #     for j in p:
    #         if i > j-1 and i < j + 20:
    #             MBe.append(str(j))
    for k,v in out.items():
        if v.split()[0] in MBe:
            print '>' + v.split()[0] + '\n' + k

inp.close()
outp.close()
