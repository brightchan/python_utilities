# Miscellaneous python utilities #

  Miscellaneous python utilities for various purpose.  
  
  1. Measuring body axis length of medaka embryos via lateral view pictures.  
  2. Measuring fluorescent signal strength from pictures.  
  3. Barchart plotting for my thesis.  
  4. Finding sequences matching requirements for molecular beacon design.