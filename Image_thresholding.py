# -*- coding: utf-8 -*-
"""
Created on Sat Mar 12 22:23:37 2016

@author: Bright
"""
from PIL import Image
from os import listdir
import matplotlib.pyplot as plt
import numpy as np
from skimage import data
from skimage.filters import threshold_otsu, threshold_adaptive, threshold_li
from skimage.filters.rank import threshold_percentile
from skimage import io
from skimage.morphology import skeletonize, medial_axis, binary_opening, binary_closing, disk, octagon


def image_thresh(image):
    thresh = threshold_otsu(image)
    otsu = image > thresh
    thresh = threshold_li(image)
    li = image > thresh
    thresh = threshold_adaptive(image, block_size=15)
    ad = image > thresh
    thresh = threshold_percentile(image, selem=disk(15), p0=0.95)
    per = image > thresh
#    sk_med = medial_axis(binary)
#    op = binary_opening(binary, octagon(2,1))
#    sk = skeletonize(op)
    fig, axes = plt.subplots(nrows=5, figsize=(25,25))
    ax0, ax1, ax2, ax3, ax4= axes
    ax0.imshow(image)
    ax0.set_title("Origin")
    ax1.imshow(otsu)
    ax1.set_title("otsu")
    ax2.imshow(li)
    ax2.set_title("li")
#    ax3.imshow(sk_med)
#    ax3.set_title("Medial_axis")
    ax3.imshow(ad)
    ax3.set_title("ad")
    ax4.imshow(per)
    ax3.set_title("per")    
    for ax in axes:
        ax.axis('off')
    plt.show()
    return(fig)
#    return(sk)

def dist(x,y):
    return np.sqrt(np.sum((x-y)**2))
   
direc = "C:/My Documents/Destop/afp/"
file = "m1b.tif"
im = io.imread(direc+file, as_grey=True)
image_thresh(im).savefig(direc+"out.tif")
#sk = image_thresh(im).T
#points = np.argwhere(sk==1)
#seq1 = points[0::len(points)//26]
#seq2 = np.vstack((seq1[1:],points[-1]))
#print(dist(seq1,seq2))


#image = data.page()
#
#global_thresh = threshold_otsu(image)
#binary_global = image > global_thresh
#
#block_size = 40
#binary_adaptive = threshold_adaptive(image, block_size, offset=10)
#
#fig, axes = plt.subplots(nrows=3, figsize=(7, 8))
#ax0, ax1, ax2 = axes
#plt.gray()
#
#ax0.imshow(image)
#ax0.set_title('Image')
#
#ax1.imshow(binary_global)
#ax1.set_title('Global thresholding')
#
#ax2.imshow(binary_adaptive)
#ax2.set_title('Adaptive thresholding')
#
#for ax in axes:
#    ax.axis('off')
#
#plt.show()