# -*- coding: utf-8 -*-
"""
Created on Sat Apr 16 22:23:37 2016

@author: Bright

Return pixel length of the bright region of an image with skeleton algorithm. 
Can be used to analyse body axis length of embryos. 

"""


from os import listdir
import numpy as np
from skimage.filters import threshold_otsu
from skimage import io
from skimage.morphology import skeletonize, disk, binary_opening, binary_closing, dilation
import matplotlib.pyplot as plt

def thresh_skl(image):# Gain the threshold and skeleton of the image with various methods
    thresh = threshold_otsu(image)
    binary = image > thresh #gain binary of the thresholded image
    cl = binary_closing(binary, octagon(2,2))
    op = binary_opening(cl, disk(6))
    sk = skeletonize(op)
    sk_di = dilation(sk, disk(12))
    
    fig, axes = plt.subplots(nrows=3, figsize=(18,50))
    ax0, ax1, ax2= axes
    ax0.imshow(binary, cmap="gray")
    ax0.set_title("Binary")
    ax1.imshow(op, cmap="gray")
    ax1.set_title("Close-open")
    ax2.imshow(sk_di, cmap="gray")
    ax2.set_title("Skeleton")
    for ax in axes:
        ax.axis('off')
    fig.savefig(direc+filepre+str(i)+"_o"+filetype)
    
    return(sk)

def dist(x,y):
    return np.sqrt(np.sum((x-y)**2))
    
def skl_length(image):#taking point sequences from left to right, cal and sum the Euclidean dist btw each
    points = np.argwhere(image==1)
    seq1 = points[0::len(points)//30]#pick 30 points, including the leftmost. sorted already
    seq2 = np.vstack((seq1[1:],points[-1]))#pick another 30 points with 29 overlaping seq1, and the rightmost.
    return(dist(seq1,seq2))
   
direc = "C:/My Documents/Destop/afp/"
filepre = "c"
filenum = 4 
filetype = ".tif"
for i in range(1,filenum+1):
    im = io.imread(direc+filepre+str(i)+filetype, as_grey=True)
    print(skl_length(thresh_skl(im).T))
    
#    skl = thresh_skl(im)
#    io.imsave(direc+filepre+str(filenum)+"_sk"+filetype, skl)
#    skl = skl   
#    skl_length(skl)