'''
Created on May 21, 2015

@author: Bright
'''
from PIL import Image
from os import listdir
from statistics import mean,stdev
import numpy as np

#calculate the average intensity of green and red and their ratio
#set input dir of MO and DNA with files of cut images of names like:DNA060mR.png, MO_180mG.png
def g_r_ratio(direc):
    #averaging the background with BG.png or BR.png, green & red channel respectively
    bgr = Image.open(direc+'BG.png').convert('L')
    bg = sum(list(bgr.getdata()))/float(bgr.size[0]*bgr.size[1])
    bre = Image.open(direc+'BR.png').convert('L')
    br = sum(list(bre.getdata()))/float(bre.size[0]*bre.size[1])
    
    #avr[] store the time, green and red value with stdev and ratio with stdev
    avr = [[] for i in range(7)]
    filename = [f for f in listdir(direc) if f[-3:]=='png' and f[0]!='B']
    #g & r store all pixel intensity of green or red    
    g = []
    r = []
    for f in filename:
        im = Image.open(direc+f).convert('L')
        d = list(im.getdata())
        if f[-5] == 'G':
            avr[0].append(f[4:-5])
            avr[1].append(mean(d)-bg)
            #avr[2].append(stdev([x-bg for x in d]))
            g.append([x-bg for x in d])
        else:
            avr[3].append((mean(d)-br)*0.4)
            #avr[4].append(stdev([x-br for x in d]))
            r.append([x-br for x in d])
    #print the result for checking
    for i in range(len(avr[0])):
        ratio = avr[1][i]/avr[3][i]
        avr[5].append(ratio)
        print(avr[0][i]+' '+str(avr[1][i])+' '+ str(avr[3][i]) + ' '+ str(ratio)[:4])
    #calculate the stdev or each g/r ratio by pixel
    for i in range(len(g)):
        rt = [g[i][j]/float(r[i][j]) for j in range(len(g[i]))]
        avr[6].append(stdev(rt))
    #output to file
    header = 'Time\t Green Intensity\t Green Stdev\t Red Intensity\t Red Stdev\t Green/Red ratio\t Ratio Stdev'    
    with open(direc + 'output_data.txt','w') as out:
        out.write(header)
        for i in range(len(avr[0])):
            out.write('\n')
            for j in range(len(avr)):
                out.write(str(avr[j][i])+'\t')
                
                
if __name__ == '__main__':
    dirMO = 'C:/Research/Injection Photos/May16 dndWT DNA MO stability/Center brightness/cut/MO/'
    dirDNA = 'E:/cjb/Sep 18 2015 FAM Rx2 444 primer & Fluorescein standard into 1 cell & 1MM target/Fluorescein/Time/'

    g_r_ratio(dirDNA)    

























'''
#unuseful obsolete codes    
    dirMO = 'C:/Research/Injection Photos/May16 dndWT DNA MO stability/Center brightness/cut/MO/'
    MOfilename = [f for f in listdir(dirMO) if f[-3:]=='png' and f[0]!='B']
    dirDNA = 'C:/Research/Injection Photos/May16 dndWT DNA MO stability/Center brightness/cut/DNA/'
    DNAfilename = [f for f in listdir(dirDNA) if f[-3:]=='png' and f[0]!='B']  
  
    #averaging the background with BG.png or BR.png, green & red channel respectively
    mobg = Image.open(dirMO+'BG.png').convert('L')
    MOBG = sum(list(mobg.getdata()))/float(mobg.size[0]*mobg.size[1])
    mobr = Image.open(dirMO+'BR.png').convert('L')
    MOBR = sum(list(mobr.getdata()))/float(mobr.size[0]*mobr.size[1])
    
    dnabg = Image.open(dirDNA+'BG.png').convert('L')
    DNABG = sum(list(dnabg.getdata()))/float(dnabg.size[0]*dnabg.size[1])
    dnabr = Image.open(dirDNA+'BR.png').convert('L')
    DNABR = sum(list(dnabr.getdata()))/float(dnabr.size[0]*dnabr.size[1])
    
    #avr[] store the time, green and red value with stdev and ratio with stdev
    avrMO = [[] for i in range(7)]
    avrDNA = [[] for i in range(7)]
    #MO[0] for background subtracted image data of green and [1] for red
    MO = [[],[]]
    DNA = [[],[]]      
    for f in MOfilename:
        im = Image.open(dirMO+f).convert('L')
        d = list(im.getdata())
        if f[-5] == 'G':
            avrMO[0].append(f[2:-6])
            avrMO[1].append(mean(d)-MOBG)
            avrMO[2].append(stdev([x-MOBG for x in d]))
            MO[0].append([x-MOBG for x in d])
        else:
            avrMO[3].append(mean(d)-MOBR)
            avrMO[4].append(stdev([x-MOBR for x in d]))
            MO[1].append([x-MOBR for x in d])
    for i in range(len(avrMO[0])):
        ratio = avrMO[1][i]/avrMO[3][i]
        avrMO[5].append(ratio)
        print(avrMO[0][i]+' '+str(avrMO[1][i])+' '+str(avrMO[2][i]) + ' ' +str(avrMO[3][i]) + ' ' +str(avrMO[4][i]) + ' '+ str(ratio)[:4])
    
    
    for f in DNAfilename:
        im = Image.open(dirDNA+f).convert('L')
        d = list(im.getdata())
        if f[-5] == 'G':
            avrDNA[0].append(f[3:-6])
            avrDNA[1].append(mean(d)-DNABG)
            avrDNA[2].append(stdev([x-DNABG for x in d]))
            DNA[0].append([x-DNABG for x in d])
        else:
            avrDNA[3].append(mean(d)-DNABR)
            avrDNA[4].append(stdev([x-DNABR for x in d]))
            DNA[1].append([x-DNABR for x in d])
    for i in range(len(avrDNA[0])):
        ratio = avrDNA[1][i]/avrDNA[3][i]
        avrDNA[5].append(ratio)
        print(avrDNA[0][i]+' '+str(avrDNA[1][i])+' '+str(avrDNA[2][i]) + ' ' +str(avrDNA[3][i]) + ' ' +str(avrDNA[4][i]) + ' '+ str(ratio)[:4])
        
    for i in range(len(MO[0])):
        MOrt = [MO[0][i][j]/float(MO[1][i][j]) for j in range(len(MO[0][i]))]
        avrMO[6].append(stdev(MOrt))
        DNArt = [DNA[0][i][j]/float(DNA[1][i][j]) for j in range(len(DNA[0][i]))]
        avrDNA[6].append(stdev(DNArt))
    
    #output to file
    fMO = open(dirMO + 'MOdata.txt','w')
    header = 'Time\tGreen Intensity\tGreen Stdev\tRed Intensity\tRed Stdev\tGreen/Red ratio\tRatio Stdev'
    fMO.write(header)
    for i in range(len(avrMO[0])):
        fMO.write('\n')
        for j in range(len(avrMO)):
            fMO.write(str(avrMO[j][i])+'\t')
    fDNA = open(dirDNA + 'DNAdata.txt','w')
    fDNA.write(header)
    for i in range(len(avrDNA[0])):
        fDNA.write('\n')
        for j in range(len(avrDNA)):
            fDNA.write(str(avrDNA[j][i])+'\t')   
    fMO.close() 
    fDNA.close()
'''