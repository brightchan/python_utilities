'''
Created on May 18, 2015

@author: Bright
For plotting in my thesis
'''

import matplotlib.pyplot as plt
import numpy as np

dir = 'c:\\users\\bright\\Google Drive\\Summary\\AFP\\'
#MOdata = np.genfromtxt(dir+'MOdata 240.csv',delimiter='\t').transpose()
#DNAdata = np.genfromtxt(dir+'DNAdata 240 (150).csv',delimiter='\t').transpose()
data = np.genfromtxt(dir+'1ng_MO_Count2.csv',delimiter='\t')
#print(data)

def plot_bar_line(MOdata):
    ind = np.arange(8)
    wid = 0.5
    cG = '#66CC99'
    cR = '#FF99CC'
    cB = '#2B56BA'
    Gr = '#938890'
    fs = 22 #fontsize
    pd = 15 #pad
    
    MOfig = plt.Figure(dpi=600,facecolor='none')
    #plot intensity
    MOcolor = plt.axes()
    plt.bar(ind,MOdata[1][1:],yerr=MOdata[2][1:], width = wid, ec = Gr, 
        fc= cG, ecolor=Gr, alpha = 1)
    plt.bar(ind,MOdata[3][1:],yerr=MOdata[4][1:], width = wid, ec = Gr, 
        fc= cR, ecolor=Gr, edgecolor = 'none', alpha = 1)
    plt.xticks(ind+wid/2.,('15','30','45','60','120','150','180','240'))
    MOcolor.set_ylabel('Intensity',fontsize = fs)
    MOcolor.set_ylim(0,160)
    
    #plot ratio
    MOratio = MOcolor.twinx()
    plt.plot(ind+wid/2,MOdata[5][1:], marker = 'D', color=cB, mec = cB, ms = 8, 
             mew=2, mfc =cB, linewidth = 2, alpha = 1)
    MOratio.set_ylim(1.6,2.8)
    MOratio.set_ylabel('Ratio',fontsize=fs, color=cB, rotation=270, 
                       labelpad=22)
    MOcolor.set_xlabel('Time/min', fontsize=fs)
    MOcolor.tick_params(axis='x', pad=pd)
    MOcolor.tick_params(axis='y', pad=pd)
    MOratio.tick_params(axis='y', pad=pd)
    plt.xlim(-0.5, 8)
    
    
    #set label font and bg
    for label in MOcolor.get_xticklabels() + MOcolor.get_yticklabels():
        label.set_fontsize(fs)
        label.set_bbox(dict(facecolor='white', edgecolor='None', alpha=1 ))
    for label in MOratio.get_yticklabels():
        label.set_fontsize(fs)
        label.set_color(cB)
        label.set_bbox(dict(facecolor='white', edgecolor='none', alpha=1 ))
    
    #set spines
    #MOcolor.spines['top'].set_color('none')
    MOcolor.spines['right'].set_color('none')
    MOcolor.xaxis.set_ticks_position('bottom')
    MOcolor.tick_params(axis='x', bottom='off')
    
    MOratio.spines['top'].set_color('none')
    MOratio.spines['right'].set_color(cB)
    MOratio.spines['left'].set_color('none')
    MOratio.xaxis.set_ticks_position('bottom')
    MOratio.tick_params(axis='y', colors=cB)
    MOratio.tick_params(axis='x', bottom='off')
    
    plt.tight_layout()


def plot_pect_bar(data):#plot normal,abnormal and death percentage
    wid = 0.21
    spc = 0.07
    cG = '#66CC99'
    cR = '#FF99CC'
    cB = '#3964CA'
    Gr = '#bec8d7'   
    xticks = ['1dpf','5dpf','3dph']  
    ind = np.arange(3)
    #creat pectage array from data
    pect = [[] for i in range(len(data))]
    for i in range(1,len(data)):
        s = sum(data[i][1:])
        for j in range(1,len(data[i])):
            pect[i].append(data[i][j]/s*100)
    pect = np.transpose(pect[1:])
    print(pect)
    #plot normal
    fig = plt.Figure(dpi=600, facecolor='none')
    p = plt.axes()
    
    for i in range(3):
        plt.bar(ind+i*wid+i*spc,pect[2][i::3],label='Normal', width = wid, fc= cG,
                edgecolor = 'white', alpha = 1)
        #add value on bar
#        for j in zip(ind+i*wid+i*spc,pect[2][i::3]):
#            plt.text(j[0]+0.18*wid,45,'%.1f'%j[1], fontsize=14, weight=540)
            #add xticks
            #plt.text(j[0]+0.1*wid,-4,xticks[i])
        #plot abnormal
        plt.bar(ind+i*wid+i*spc,pect[1][i::3],bottom=pect[2][i::3],label='Abnormal',
                width = wid, fc= Gr, edgecolor = 'white', alpha = 1)
        #plot dead
        plt.bar(ind+i*wid+i*spc,pect[0][i::3],bottom=[pect[1][j]+pect[2][j] for j in range(i,9,3)],
                label='Dead', width = wid, fc= cR,edgecolor = 'white', alpha = 0.6)
    plt.xlim(-0.2,2.95)
    p.set_ylabel('Percentage/%',fontsize = 20)
    p.set_xticks([],[])
    p.spines['right'].set_color('none')
    p.spines['top'].set_color('none')
    p.tick_params(axis='y', right='off')
    #set tick size
    for tick in p.yaxis.get_major_ticks():
                tick.label.set_fontsize(14) 
    
if __name__ == '__main__':
    #plot_bar_line(DNAdata)
    plot_pect_bar(data)
    plt.show()